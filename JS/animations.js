﻿$(".categories-trigger").click(function () {
    $(".categories-trigger").toggleClass("mvd");
    if ($(".categories-trigger").hasClass("mvd")) {
        $(".categories-trigger span").html(' <i class="fa fa-arrow-up" aria-hidden="true"></i>');

    } else {
        $(".categories-trigger span").html(' <i class="fa fa-arrow-down" aria-hidden="true"></i>');

    }

    $(".categories").toggleClass("down");
    $(".row").toggleClass("mvd");

});